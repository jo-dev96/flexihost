salt_master_packages:
  pkg.installed:
    - pkgs:
      - salt-common
      - salt-master
      # No need for Python 2 on newer Salt versions
      - python3-pygit2
      - python3-tornado
      - python3-docker
      - git

# We'll add some salt-cloud stuff here too (In the future)

salt_master_configuration:
  user.present:
    - name: salt
    - home: /var/lib/salt
    - shell: /bin/sh

  group.present:
    - name: salt

salt_master_base_configuration:
  file.managed:
    - name: /etc/salt/master.d/base.conf
    - source: salt://salt_master/files/etc/salt/master.d/base.conf.jinja
    - makedirs: True
    - template: jinja

salt_master_enable:
  service.enabled:
    - name: salt-master

salt_master_start:
  service.running:
    - name: salt-master

# Firewall config
salt_ufw_enable:
  cmd.run:
    - name: "ufw allow salt"