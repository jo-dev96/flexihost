ufw_packages:
  pkg.installed:
  - pkgs:
    - ufw

# Copy ufw application profiles from Salt
ufw.copy_application_profiles:
  file.recurse:
    - name: /etc/ufw/applications.d/
    - source: salt://ufw/files/etc/ufw/applications.d

# Enable ufw default deny
ufw.default_deny_inbound:
  cmd.run:
    - name: "ufw default deny incoming"

# Enable SSH inbound by default
ufw.allow_inbound_ssh:
  cmd.run:
    - name: "ufw allow ssh"

# Enable ufw
ufw.enable_ufw:
  cmd.run:
    - name: "ufw --force enable"