!!! ONLY ADD CORE APPLICATIONS SUCH AS SSH AND SALT INTO THIS MODULE !!!
This module is used for configuring UFW. All profiles and settings in this module will be copied to ALL minions.

SSH and Salt MUST be left alone. These are required above all others.