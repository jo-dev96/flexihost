unattended_upgrade_install:
  pkg.installed:
    - pkgs:
      - unattended-upgrades

unattended_upgrade_config:
  file.managed:
    - name: /etc/apt/apt.conf.d/50unattended-upgrades
    - source: salt://ubuntu_unattended_upgrades/files/etc/apt/apt.conf.d/50unattended-upgrades