docker_packages:
  pkg.installed:
    - pkgs:
      - docker
      - docker.io
      - python-docker

docker_service_enable:
  service.enabled:
    - name: docker

docker_service_running:
  service.running:
    - name: docker