base:
  '*':
    - ufw
    # Disable this in the future whe we implement containers, only for traditional installations
    - ubuntu_unattended_upgrades
  'j-ude-vm-01':
    - salt_master
  'j-ude-vm-02':
    - docker_base
    - abstruse_ci