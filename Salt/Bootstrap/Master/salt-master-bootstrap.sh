# Currently Ubuntu/Debian only, feel free to fix this ;)
###
# Simple script to bootstrap a salt master install
###

#!/bin/bash

echo "Run this script as root..."
apt install git curl python3-pygit2 python-pygit2 salt-minion salt-master salt-cloud -y

# Some fucking stupid URL shit. GitLab forced API usage is shit!!!
curl -o /etc/salt/minion.d/master-init.conf "https://gitlab.com/api/v4/projects/14035506/repository/files/Salt%2FBootstrap%2FMaster%2Fetc%2Fsalt%2Fminion/raw?ref=master"
systemctl disable salt-minion
systemctl restart salt-minion