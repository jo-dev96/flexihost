# Currently Ubuntu/Debian only, feel free to fix this ;)
###
# Simple script to bootstrap a salt minion install
###

#!/bin/bash

echo "Run this script as root..."
apt install git curl python3-pygit2 python-pygit2 salt-minion salt-common -y

# Some fucking stupid URL shit. GitLab forced API usage is shit!!!
curl -o /etc/salt/minion.d/base.conf "https://gitlab.com/api/v4/projects/14035506/repository/files/Salt%2FBootstrap%2FMinion%2Fetc%2Fsalt%2Fminion/raw?ref=master"
systemctl enable salt-minion --now
systemctl restart salt-minion